-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Okt 2023 pada 08.25
-- Versi server: 8.1.0
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvc_travel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blogs`
--

CREATE TABLE `blogs` (
  `id_blog` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `image` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `category` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `blogs`
--

INSERT INTO `blogs` (`id_blog`, `title`, `image`, `content`, `category`) VALUES
(1, 'Memulai Pagi dengan Energik: 5 Kebiasaan Sehat yang Harus Anda Coba', '1698625734-abc38376a77af1c756221454f1812494.jpg', '<div><br>Gaya hidup sehat adalah kunci untuk menjalani hari dengan semangat dan vitalitas. Ada banyak kebiasaan yang dapat Anda terapkan untuk memastikan Anda memulai pagi dengan energi positif. Di bawah ini adalah beberapa tips yang mungkin ingin Anda pertimbangkan:<br><br></div><ol><li><strong>Bangun Pagi dan Berolahraga</strong>: Cobalah untuk bangun lebih awal dan lakukan sedikit olahraga. Bahkan hanya 15 menit berjalan kaki atau peregangan bisa memberi Anda dorongan energi yang diperlukan.</li><li><strong>Sarapan Sehat</strong>: Sarapan adalah makanan terpenting sepanjang hari. Pastikan Anda memiliki sarapan yang kaya akan nutrisi, seperti oatmeal dengan buah-buahan segar atau telur rebus dengan sayuran.</li><li><strong>Minum Air Putih</strong>: Jangan lupa untuk minum cukup air. Air membantu menjaga tubuh tetap terhidrasi dan meningkatkan konsentrasi.</li><li><strong>Meditasi atau Yoga</strong>: Sejenak meditasi atau latihan yoga ringan di pagi hari dapat membantu merilekskan pikiran dan tubuh Anda.</li><li><strong>Rencanakan Hari Anda</strong>: Ambil beberapa menit untuk merencanakan tugas dan tujuan Anda untuk hari ini. Ini akan membantu Anda tetap fokus dan produktif.</li></ol><div><br>Semua langkah ini dapat membantu Anda memulai pagi dengan energi positif dan meningkatkan kualitas hidup Anda. Ingatlah bahwa perubahan kecil dalam kebiasaan sehari-hari dapat memiliki dampak besar pada kesejahteraan Anda.<br><br></div>', 'Lifestyle'),
(2, '5 Tips Penting untuk Meningkatkan Kinerja Lari Anda', '1698625995-bg contact.jpg', '<div><br>Apakah Anda seorang pelari pemula yang ingin meningkatkan kinerja Anda atau pelari berpengalaman yang ingin mencapai target yang lebih tinggi? Di sini, kami akan memberikan beberapa tips penting yang dapat membantu Anda meningkatkan kinerja lari Anda:<br><br></div><ol><li><strong>Rencanakan Latihan Anda</strong>: Setel jadwal latihan yang konsisten. Rencanakan waktu dan jarak lari Anda, dan perbarui rencana Anda secara berkala.</li><li><strong>Variasi Latihan</strong>: Cobalah untuk memasukkan latihan berintensitas tinggi, seperti latihan interval atau latihan kekuatan, untuk memecah rutinitas dan meningkatkan kinerja Anda.</li><li><strong>Nutrisi yang Tepat</strong>: Pastikan Anda mendapatkan nutrisi yang cukup, termasuk karbohidrat, protein, dan lemak sehat, untuk mendukung energi dan pemulihan.</li><li><strong>Recovery yang Adekuat</strong>: Beri tubuh Anda waktu untuk pulih setelah latihan yang berat. Ini termasuk tidur yang cukup dan pemulihan aktif, seperti peregangan.</li><li><strong>Tetap Termotivasi</strong>: Tetapkan tujuan yang realistis dan jelas, serta cari dukungan dari rekan pelari atau komunitas lari lokal.</li></ol><div><br>Dengan menerapkan tips ini, Anda dapat meningkatkan kinerja lari Anda dan mencapai hasil yang lebih baik. Ingatlah bahwa kesuksesan dalam lari memerlukan komitmen dan ketekunan. Semoga Anda mencapai target lari Anda!<br><br></div>', 'Sport'),
(3, 'Eksplorasi Wisata Kuliner di Kota Baru: Petualangan Kuliner Terbaik Anda', '1698626171-portrait-from-back-female-model-pink-dress-looking-rain-forest-outdoor-shot-graceful-woman-dancing-near-pool.jpg', '<div><br>Ketika kita berbicara tentang perjalanan, pengalaman kuliner adalah bagian penting yang tidak boleh dilewatkan. Saat Anda mengunjungi kota baru, salah satu cara terbaik untuk merasakan budaya lokal adalah melalui makanan. Berikut adalah beberapa petualangan kuliner yang harus Anda coba ketika mengunjungi Kota Baru:<br><br></div><ol><li><strong>Jajal Hidangan Lokal di Pasar Tradisional</strong>: Kunjungi pasar tradisional di pagi hari dan nikmati hidangan sarapan lokal. Cobalah makanan seperti nasi uduk atau bubur ayam, dan rasakan keanekaragaman rasa yang ditawarkan.</li><li><strong>Eksplorasi Restoran Lokal</strong>: Temukan restoran lokal yang melayani masakan khas Kota Baru. Cobalah hidangan khas seperti rendang, soto, atau nasi goreng.</li><li><strong>Food Truck dan Jajanan Jalanan</strong>: Jelajahi food truck dan pedagang jajanan jalanan. Makanan jalanan seringkali merupakan tempat terbaik untuk menemukan hidangan lezat dengan harga terjangkau.</li><li><strong>Kuliner Etnik</strong>: Kota Baru seringkali memiliki berbagai restoran yang melayani masakan etnik. Cobalah masakan dari berbagai budaya, seperti masakan India, Tionghoa, atau Arab.</li><li><strong>Kafe Khas</strong>: Jangan lupa untuk mampir ke kafe khas yang menawarkan kopi lokal dan kudapan yang lezat. Ini adalah tempat yang sempurna untuk bersantai sambil menikmati suasana kota.</li></ol><div><br>Selama perjalanan kuliner Anda, jangan lupa untuk berbicara dengan penduduk setempat dan meminta rekomendasi kuliner terbaik. Makanan adalah cara yang sempurna untuk merasakan keunikan kota yang Anda kunjungi. Selamat menikmati!<br><br></div>', 'Travel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$vOzt9hqmlkMo.DjOyT4Sb.q.dR0L6Rho5IRIpncPHDEqyWpDKqreK'),
(2, 'admin', 'admin@admin.com', '$2y$10$cXN1Ss935EY9276Ifg0aoeqNf..NjvT2ACwqYu6FcBsoQmhpCUss6');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id_blog` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
