<?php

class Blog extends Controller
{
    public function index()
    {
        $data['blog'] = $this->model('Blog_model')->all();
        $data['title'] = 'Blog';
        $data['blog-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/nav', $data);
        $this->view('blog/index', $data);
        $this->view('templates/footer');
    }
    public function detail($id_blog)
    {
        $data['title'] = 'Detail Blog';
        $data['blog'] = $this->model('Blog_model')->getBlogById($id_blog);
        $data['blog-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/nav', $data);
        $this->view('blog/detail', $data);
        $this->view('templates/footer');
    }
}
