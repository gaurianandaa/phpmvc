<?php

class Login extends Controller
{
    public function index()
    {
        $data['title'] = 'Login';
        $this->view('templates/header', $data);
        $this->view('login/index');
    }
    public function auth()
    {
        if ($this->model('User_model')->login($_POST) > 0) {
            $_SESSION['login'] = true;
            header('location: ' . BASEURL . '/home');
        } else {
            echo '<script>';
            echo 'alert("Login gagal. Silakan cek kembali informasi login Anda.");';
            echo 'window.location.href = "' . BASEURL . '/login";'; // Jika ingin mengarahkan pengguna ke halaman login setelah menutup alert
            echo '</script>';
        }
    }
    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
