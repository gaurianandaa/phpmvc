<?php

class Register extends Controller
{
    public function index()
    {
        $data['title'] = 'Register';
        $this->view('templates/header',$data);
        $this->view('register/index');
        $this->view('templates/footer');
    }

    public function create()
    {
        // var_dump($_POST);
        // die;
        if ($this->model('User_model')->register($_POST) > 0) {
            header('Location: ' . BASEURL . '/login');
        }
    }
}
