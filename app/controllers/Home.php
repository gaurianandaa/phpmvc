<?php

class Home extends Controller
{

    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('Location: ' . BASEURL . '/login');
        }
    }
    public function index()
    {

        $data['title'] = 'Home';
        $data['home-active'] = true;
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('templates/header', $data);
        $this->view('templates/nav', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }
}
