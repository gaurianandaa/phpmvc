<?php

class Admin extends Controller
{
    public function index()
    {
        $data['title'] = 'Admin';
        $data['admin-active'] = true;
        $data['data'] = $this->model('Blog_model')->all();
        $this->view('templates/header', $data);
        $this->view('templates/nav', $data);
        $this->view('admin/index', $data);
        $this->view('templates/footer');
    }
    public function create()
    {
        $data['title'] = 'Admin';
        $data['admin-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/nav', $data);
        $this->view('admin/create', $data);
        $this->view('templates/footer');
    }

    public function prosesCreate()
    {
        if ($this->model('Blog_model')->createBlog($_POST) > 0) {
            echo '<script>';
            echo 'alert("Blog anda berhasil di posting!");';
            echo 'window.location.href = "' . BASEURL . '/admin"';
            echo '</script>';
        } else {
            header('Location: ' . BASEURL . '/admin/create');
        }
    }
}
