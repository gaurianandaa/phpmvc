<?php

class Blog_model
{
    private $table = 'blogs';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }
    public function getBlogById($id_blog)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_blog = :id_blog');
        $this->db->bind('id_blog', $id_blog);
        return $this->db->single();
    }
    public function createBlog($data)
    {
        if (isset($data['title']) && isset($data['content']) && isset($data['category']) && isset($_FILES['image']['name'])) {
            $title = $data['title'];
            $content = $data['content'];
            $category = $data['category'];
            $image_name = $_FILES['image']['name'];
            $image_tmp = $_FILES['image']['tmp_name'];

            $allowedExt = array("jpg", "png", "jpeg", "svg");
            $extGambar = pathinfo($image_name, PATHINFO_EXTENSION);

            if (in_array($extGambar, $allowedExt)) {
                $uniqueFileName = time() . '-' . $image_name;

                $upload_path = '../public/img/' . $uniqueFileName;

                if (move_uploaded_file($image_tmp, $upload_path)) {
                    $query = "INSERT INTO blogs (title, content, category, image) VALUES(:title, :content, :category, :image)";
                    $this->db->query($query);
                    $this->db->bind('title', $title);
                    $this->db->bind('content', $content);
                    $this->db->bind('category', $category);
                    $this->db->bind('image', $uniqueFileName);

                    $this->db->execute();
                    return 1;
                } else {
                    return -1; // Error in file upload
                }
            } else {
                return -2; // Invalid file extension
            }
        } else {
            return -3; // Missing data keys
        }
    }
}
