<div class="container my-5">
    <div class="row">
        <div class="d-flex justify-content-center">
            <div class="col-lg-10">
                <div class="text-center">
                    <h3>"<?= $data['blog']['title']; ?>"</h3>
                    <div class="image my-4">
                        <img src="<?= HREF;?>/img/<?=$data['blog']['image']?>" alt="" style="width: 100%; height:25rem;">
                        <p class="text-secondary fst-italic mt-2"><?= $data['blog']['image'] ?></p>
                    </div>
                    
                </div>
                <div class="text-start">
                    <p class="text-secondary"><?= $data['blog']['content']; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>