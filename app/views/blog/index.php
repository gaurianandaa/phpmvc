<div class="container">
    <div class="row">
        <?php foreach ($data['blog'] as $blog) : ?>
            <?php if ($blog['id_blog'] === 1) : ?>
                <div class="col-lg-9 my-4">
                    <img src="<?= HREF ?>/img/<?= $blog['image'] ?>" alt="" class="img-fluid rounded" style="width: 100%; height: 20rem; object-fit: cover;">
                    <div class="my-3 px-3 py-1 fw-semibold text-danger rounded-5" style="font-size: 12px; width: fit-content; background-color:#f1efef;">
                                <?= $blog['category'] ?>
                    </div>
                    <h2 class="text-abu fw-bold mt-2"><?= $blog['title'] ?></h2>
                    <p class="text-abu"><?= $blog['content'] ?></p>
                    <button class="btn bg-active px-3 py-2 text-white">
                        Read more
                    </button>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <div class="col-lg-3 my-4">
            <?php foreach ($data['blog'] as $blog) : ?>
                <?php if ($blog['id_blog'] !== 1) : ?>
                    <div class="card mb-3  border border-0 shadow-lg" style="width: 18rem;">
                        <img src="<?= HREF ?>/img/<?= $blog['image  ']?>" class="card-img-top" alt="..." style="height: 150px;">
                        <div class="card-body pt-0"> 
                            <div class="my-3 px-3 py-1 fw-semibold text-danger rounded-5" style="font-size: 12px; width: fit-content; background-color:#f1efef;">
                                <?= $blog['category'] ?>
                            </div>
                            <h6 class="card-title"><?= substr($blog['title'], 0, 50); ?>...</h6>
                            <!-- <h6 class="card-text text-secondary fw-light"><?= substr($blog['content'], 0, 50); ?>...</h6> -->
                            <button class="btn btn-primary rounded-5 btn-sm" style="width: fit-content;">
                                <a href="<?= HREF; ?>/blog/detail/<?= $blog['id_blog'] ?>" class="text-light text-decoration-none">
                                    Read More
                                </a>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>