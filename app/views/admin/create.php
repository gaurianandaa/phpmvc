<div class="container my-5">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-8 col-lg-8 mg-t-10 mg-lg-t-0">
            <div class="card shadow-lg border-0">
                <div class="card-header">
                    <h5 id="section2" class="mg-b-10">Create Blog</h5>
                </div>
                <div class="card-body p-4">
                    <form action="<?= BASEURL; ?>/admin/prosesCreate" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mt-3">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title" id="title" required>
                                </div>

                                <div class="form-group mt-3">
                                    <label for="category">Category</label>
                                    <select class="form-select" name="category" id="category" required>
                                        <option value="" selected>Select Category</option>
                                        <option value="Technology">Technology</option>
                                        <option value="Sport">Sport</option>
                                        <option value="Travel">Travel</option>
                                        <option value="Lifestyle">Lifestyle</option>
                                    </select>
                                </div>
                                <div class="form-group mt-3">
                                    <label for="">Description</label>
                                    <input id="x" type="hidden" name="content" required>
                                    <trix-editor input="x"></trix-editor>
                                </div>
                                <!-- <div class="form-group mt-3">
                                    <label for="">Description</label>
                                    <textarea name="content" id="x" cols="30" rows="10"></textarea>
                                    <trix-editor input="x"></trix-editor>
                                </div> -->
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mt-3">
                                    <label for="image">Image</label>
                                    <div id="drop-area" class="d-flex flex-column justify-content-center align-items-center" style="height: 43vh;">
                                        <img id="previewImage" width="200">
                                        <input type="file" id="image" accept="image/" class="opacity-0" name="image">
                                        <div class="drop-content text-center">
                                            <h5>Drag files to upload</h5>
                                            <p class="text-secondary">-or select a file-</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn bg-active text-light mt-3">
                                <i class="fa-solid fa-floppy-disk"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


<style>
    #drop-area {
        border: 2px dotted #ccc;
        text-align: center;
        cursor: pointer;
    }
</style>

<script>
    const dropArea = document.getElementById('drop-area');
    const imageInput = document.getElementById('image');
    const previewImage = document.getElementById('previewImage');

    imageInput.addEventListener('change', function() {
        const file = imageInput.files[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = function(e) {
                previewImage.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            previewImage.src = '';
        }
    })

    dropArea.addEventListener('dragover', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #000';
    });

    dropArea.addEventListener('dragleave', () => {
        dropArea.style.border = '2px dashed #ccc';
    });

    dropArea.addEventListener('drop', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #ccc';
        const file = e.dataTransfer.files[0];
        imageInput.files = e.dataTransfer.files;
    });

    dropArea.addEventListener('click', () => {
        imageInput.click();
    });
</script>