<nav class="navbar navbar-expand-lg bg-white">
    <div class="container d-flex justify-content-center align-items-center ">

        <a class="navbar-brand fw-bold" href="<?= BASEURL; ?>"><img src="<?= HREF?>/img/icon-512 2.svg" alt=""> Destinize</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse d-flex" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 justify-content-center  w-100">
                <li class="nav-item">
                    <a class="nav-link <?= isset($data['home-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= isset($data['about-active']) ? 'text-active' : '' ?>" href="<?= BASEURL; ?>/about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= isset($data['package-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/package">Package</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= isset($data['blog-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/blog">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= isset($data['admin-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/admin">Admin</a>
                </li>
            </ul>
            <button class="btn bg-active">
                <a href="<?= BASEURL; ?>/login/logout" onclick="return confirm('yakin logout?')" class="text-decoration-none text-light">logout</a>
            </button>
        </div>
    </div>
</nav>