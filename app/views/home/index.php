 <?php
    if (!$_SESSION['login']) {
        header('Location: ' . BASEURL . '/login');
    }

?>

 <div class="container">
     <div class="row my-5 mt-5 mx-5">
         <div class="col-lg-5">
             <h6 class="bg-abu text-purple px-3 py-2 rounded-4" style="width: fit-content;">✈ • Explore the wonderful indonesia!</h6>
             <h1 class="fw-bold text-abu">Liburan & nikmati <span class="text-purple">tempat baru</span> di indonesia</h1>
             <p class="text-abu">Destinize membuat kamu selalu update terkait tempat liburan baru di Indonesia dengan mengikuti perkembangan para influencer di sosial media</p>
             <button class="btn bg-active text-light py-2">
                 Mulai Sekarang
             </button>
         </div>
         <div class="col-lg-7">
             <img src="<?=HREF?>/img/Group 592 1.svg" style="width: fit-content;" alt="">
         </div>
     </div>
 </div>